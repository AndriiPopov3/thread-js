import { PostsApiPath } from '../../common/enums/enums';

const initPost = (Router, services) => {
  const { post: postService, user: userService } = services;
  const router = Router();

  router
    .get(PostsApiPath.ROOT, (req, res, next) => postService
      .getPosts(req.query)
      .then(posts => res.send(posts))
      .catch(next))
    .get(PostsApiPath.$ID, (req, res, next) => postService
      .getPostById(req.params.id)
      .then(post => {
        res.send(post);
      })
      .catch(next))
    .get(PostsApiPath.LIKELIST$ID, (req, res, next) => postService
      .getLikeListById(req.params.id)
      .then(userIds => {
        userService.getUsersByIds(userIds)
          .then(usernames => {
            return res.send(usernames);
          });
      })
      .catch(next))
    .post(PostsApiPath.ROOT, (req, res, next) => postService
      .create(req.user.id, req.body)
      .then(post => {
        req.io.emit('new_post', post); // notify all users that a new post was created
        return res.send(post);
      })
      .catch(next))
    .post(PostsApiPath.SHARE, (req, res, next) => userService.
      getUserById(req.user.id)
      .then(user => {
        postService.shareByEmail(user, req.body);
      })
      .catch(next))
    .put(PostsApiPath.EDIT$ID, (req, res, next) => postService
      .getPostById(req.params.id)
      .then(post => {
        if (post.userId === req.user.id) { // user can only update own posts
          postService.edit(req.params.id, req.body.body)
          .then(post => {
          return res.send(post);
          })
        }
      })
      .catch(next))
    .delete(PostsApiPath.$ID, (req, res, next) => postService
      .getPostById(req.params.id)
      .then(post => {
        if (post.userId === req.user.id) { // user can only delete own posts
          postService.deletePostById(req.params.id)
          .then(id => {
          return res.send(id);
          })
        }
      })
      .catch(next))
    .put(PostsApiPath.REACT, (req, res, next) => postService
      .setReaction(req.user.id, req.body)
      .then(reaction => {
        if (reaction.post && reaction.post.userId !== req.user.id) {
          if(reaction.isLike === true){
            // notify a user if someone (not himself) liked his post
          req.io
          .to(reaction.post.userId)
          .emit('like', 'Your post was liked!');
          userService.getUserById(reaction.post.userId)
          .then(user => {
            postService.notifyByEmail(reaction.post, user, req.user.username, true);
          });
          }
          if(reaction.isLike === false){
          // notify a user if someone (not himself) disliked his post
          req.io
            .to(reaction.post.userId)
            .emit('dislike', 'Your post was disliked!');
          userService.getUserById(reaction.post.userId)
          .then(user => {
          postService.notifyByEmail(reaction.post, user, req.user.username, false);
          });
          }
        }
        return res.send(reaction);
      })
      .catch(next));

  return router;
};

export { initPost };
