import { UserApiPath } from '../../common/enums/enums';

const initUser = (Router, services) => {
  const { user: userService } = services;
  const router = Router();

  router
    .post(UserApiPath.STATUS, (req, res, next) => userService
      .setStatus(req.user.id, req.body)
      .catch(next))
    .post(UserApiPath.USERNAME, (req, res, next) => userService
      .setUsername(req.user.id, req.body)
      .then(payload => {
        if (payload.userId === req.user.id) {
          if(!payload.newUsername){
          req.io
          .to(payload.userId)
          .emit('usernameExists', 'Username already exists'); // error notification
          }
        }
        return res.send(payload);
      })
      .catch(next))
    .post(UserApiPath.PHOTO, (req, res, next) => userService
      .setPhoto(req.user.id, req.body)
      .then(image => res.send(image))
      .catch(next));

  return router;
};

export { initUser };
