import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (Router, services) => {
  const { comment: commentService, user: userService } = services;
  const router = Router();

  router
    .get(CommentsApiPath.$ID, (req, res, next) => commentService
      .getCommentById(req.params.id)
      .then(comment => res.send(comment))
      .catch(next))
    .post(CommentsApiPath.ROOT, (req, res, next) => commentService
      .create(req.user.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .put(CommentsApiPath.EDIT$ID, (req, res, next) => commentService
      .getCommentById(req.params.id)
      .then(comment => {
        if (comment.userId === req.user.id) { // user can only update own posts
          commentService.edit(req.params.id, req.body.body)
          .then(comment => {
          return res.send(comment);
          })
        }
      })
      .catch(next))
    .delete(CommentsApiPath.$ID, (req, res, next) => commentService
      .getCommentById(req.params.id)
      .then(comment => {
        if (comment.userId === req.user.id) { // user can only delete own comments
          commentService.deleteCommentById(req.params.id)
          .then(id => {
          return res.send(id);
          })
        }
      })
      .catch(next))
    .get(CommentsApiPath.LIKELIST$ID, (req, res, next) => commentService
      .getLikeListById(req.params.id)
      .then(userIds => {
        userService.getUsersByIds(userIds)
          .then(usernames => {
            return res.send(usernames);
          });
      })
      .catch(next))
    .put(CommentsApiPath.REACT, (req, res, next) => commentService
      .setReaction(req.user.id, req.body)
      .then(reaction => {
        if (reaction.post && reaction.comment.userId !== req.user.id) {
          // notify a user if someone (not himself) liked his comment
          req.io
            .to(reaction.comment.userId)
            .emit('like', 'Your comment was liked!');
        }
        return res.send(reaction);
      })
      .catch(next));

  return router;
};

export { initComment };
