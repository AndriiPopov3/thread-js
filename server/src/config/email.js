import { ENV } from '../common/enums/enums';
const nodemailer = require("nodemailer");

let transporter = nodemailer.createTransport({
    host: ENV.EMAIL.EMAIL_HOST,
    port: ENV.EMAIL.EMAIL_PORT,
    secure: false,
    auth: {
      user: ENV.EMAIL.EMAIL_USERNAME,
      pass: ENV.EMAIL.EMAIL_PASSWORD
    }
  });

  export { transporter };