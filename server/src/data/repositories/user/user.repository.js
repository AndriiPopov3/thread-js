import { Abstract } from '../abstract/abstract.repository';
import { sequelize } from '../../db/connection';
const { Op } = require("sequelize");

class User extends Abstract {
  constructor({ userModel, imageModel }) {
    super(userModel);
    this._imageModel = imageModel;
  }

  addUser(user) {
    return this.create(user);
  }

  getByEmail(email) {
    return this.model.findOne({ where: { email } });
  }

  getByUsername(username) {
    return this.model.findOne({ where: { username } });
  }

  async setStatusById(userId, body) {
    const [results, metadata] = await this.model.update({
      status: body.body
    }, {
      where: { id: userId }
    });
    return body;
  }

  async setUsernameById(userId, body) {
    const [results, metadata] = await this.model.update({
      username: body.body
    }, {
      where: { id: userId }
    });
    return body;
  }

  async setPhotoById(userId, body) {
    const [results, metadata] = await this.model.update({
      imageId: body.imageId
    }, {
      where: { id: userId }
    });
    return body;
  }

  getUserById(id) {
    return this.model.findOne({
      group: ['user.id', 'image.id'],
      attributes: ['id', 'createdAt', 'email', 'updatedAt', 'username', 'status'],
      where: { id },
      include: {
        model: this._imageModel,
        attributes: ['id', 'link']
      }
    });
  }

  getUsersbyIds(ids){
    return this.model.findAll({
      where: {id: { [Op.in] : ids }}
    })
  }
}

export { User };
