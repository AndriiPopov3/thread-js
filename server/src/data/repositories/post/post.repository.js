import { sequelize } from '../../db/connection';
import { Abstract } from '../abstract/abstract.repository';
const { Op } = require("sequelize");
const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
const likeCaseComments = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class Post extends Abstract {
  constructor({
    postModel,
    commentModel,
    commentReactionModel,
    userModel,
    imageModel,
    postReactionModel
  }) {
    super(postModel);
    this._commentModel = commentModel;
    this._commentReactionModel = commentReactionModel;
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._postReactionModel = postReactionModel;
  }

  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      isLike,
      userId,
      operator
    } = filter;
    const where = {};

    if (userId && operator === undefined && isLike === undefined) {
      Object.assign(where, { userId });
    }
    if (userId && operator === "!") {
      Object.assign(where, { userId: { [Op.ne] : userId } });
    }
    if(userId && isLike){
      Object.assign(where, { id:  { [Op.in]: sequelize.literal(`(SELECT "postId"
        FROM "postReactions" AS "reaction"
        WHERE
            "reaction"."userId" = '${userId}'
            AND
            "reaction"."isLike" = true
        )`)} });
    }
    // if(userId && operator === undefined && isLike === true) {
    //   Object.assign(where, {
    //     userId: userId,
    //     id:  { [Op.in]: sequelize.literal(`(SELECT "postId"
    //   FROM "postReactions" AS "reaction"
    //   WHERE
    //       "reaction"."userId" = '${userId}'
    //       AND
    //       "reaction"."isLike" = true
    //   )`) }});
    // }
    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS null)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: this._imageModel,
        attributes: ['id', 'link']
      }, {
        model: this._userModel,
        attributes: ['id', 'username'],
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: this._postReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  async softDeletePostById(postId) {
    await this.model.destroy({
      where: {
        id: postId
      }
    });
    return { postId };
  }

  async editPostById(postId, newBody) {
    const [results, metadata] = await this.model.update({
      body: newBody
    }, {
      where: { id: postId }
    });
    return newBody;
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'comments->commentReactions.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS null)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: this._commentModel,
        attributes: {
          include: [
            // [sequelize.fn('SUM', sequelize.literal(likeCaseComments(true))), 'likeCount'],
            // [sequelize.fn('SUM', sequelize.literal(likeCaseComments(false))), 'dislikeCount']
          ]
        },
        include: [{
          model: this._userModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }}, {
          model: this._commentReactionModel,
          attributes: []
        }]
      }, {
        model: this._userModel,
        attributes: ['id', 'username'],
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: this._imageModel,
        attributes: ['id', 'link']
      }, {
        model: this._postReactionModel,
        attributes: []
      }]
    });
  }
}

export { Post };
