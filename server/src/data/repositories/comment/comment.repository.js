import { Abstract } from '../abstract/abstract.repository';

class Comment extends Abstract {
  constructor({ commentModel, userModel, imageModel, commentReactionModel }) {
    super(commentModel);
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._commentReactionModel = commentReactionModel;
  }

  async softDeleteCommentById(commentId) {
    await this.model.destroy({
      where: {
        id: commentId
      }
    });
    return { commentId };
  }

  async editCommentById(commentId, newBody) {
    const [results, metadata] = await this.model.update({
      body: newBody
    }, {
      where: { id: commentId }
    });
    return newBody;
  }

  getCommentById(id) {
    return this.model.findOne({
      group: ['comment.id', 'user.id', 'user->image.id'],
      where: { id },
      include: [
        {
          model: this._userModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        }
      ]
    });
  }
}

export { Comment };
