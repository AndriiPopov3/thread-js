import { Abstract } from '../abstract/abstract.repository';

class CommentReaction extends Abstract {
  constructor({ commentReactionModel, commentModel }) {
    super(commentReactionModel);
    this._commentModel = commentModel;
  }

  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: ['commentReaction.id', 'comment.id'],
      where: { userId, commentId },
      include: [
        {
          model: this._commentModel,
          attributes: ['id', 'userId']
        }
      ]
    });
  }

  getLikeListById(id) {
    return this.model.findAll({
      where: {commentId: id, isLike: true},
      attributes: ['userId']
    });
  }
}

export { CommentReaction };
