'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('posts', 'deletedAt', {
        type: Sequelize.DATE,
      }, { transaction })
    ]));
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('posts', 'deletedAt', { transaction })
    ]))
  }
};
