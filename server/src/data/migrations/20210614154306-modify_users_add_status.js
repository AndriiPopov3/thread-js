'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn('users', 'status', {
        type: Sequelize.STRING
      }, { transaction })
    ]));
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'status', { transaction })
    ]))
  }
};
