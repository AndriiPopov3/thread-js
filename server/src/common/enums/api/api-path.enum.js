const ApiPath = {
  AUTH: '/auth',
  POSTS: '/posts',
  COMMENTS: '/comments',
  IMAGES: '/images',
  USER: '/user'
};

export { ApiPath };
