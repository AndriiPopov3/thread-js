const CommentsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  LIKELIST$ID: '/likeList/:id',
  EDIT$ID: '/edit/:id'
};

export { CommentsApiPath };
