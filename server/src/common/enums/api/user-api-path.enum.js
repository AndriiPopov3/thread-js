const UserApiPath = {
    ROOT: '/',
    STATUS: '/status',
    USERNAME: '/username',
    PHOTO: '/photo'
  };

  export { UserApiPath };
