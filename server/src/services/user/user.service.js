class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    const user = await this._userRepository.getUserById(id);

    return user;
  }

  async getUsersByIds(ids) {
    const userList = await this._userRepository.getUsersbyIds(ids);
    let userListUsernames = [];
    for(let i=0; i<userList.length; i++){
      userListUsernames.push(userList[i].dataValues.username);
    }
    return userListUsernames;
  }

  async setStatus(userId, status) {
    const newStatus = await this._userRepository.setStatusById(userId, status);
    return newStatus;
  }

  async setUsername(userId, username) {
    const usernameExists = await this._userRepository.getByUsername(username.body);
    if (usernameExists) {
      return {userId, newUsername: null };
    } else {
      const newUsername = await this._userRepository.setUsernameById(userId, username);
      return {userId, newUsername};
    }
  }

  async setPhoto(userId, file) {
    const newPhoto = await this._userRepository.setPhotoById(userId, file);
    return newPhoto;
  }

}

export { User };
