import { transporter } from "../../config/email";

class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    return this._postRepository.getPosts(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  edit(postId, body) {
    return this._postRepository.editPostById(postId, body);
  }

  deletePostById(id) {
    return this._postRepository.softDeletePostById(id);
  }

  async getLikeListById(id){
    const likeList = await this._postReactionRepository.getLikeListById(id);
    let likeListUsernames = [];
    for(let i=0; i<likeList.length; i++){
      likeListUsernames.push(likeList[i].dataValues.userId);
    }
    return likeListUsernames;
  }

  async shareByEmail(user, body){
    let info = await transporter.sendMail({
      from: 'thread-js',
      to: `${body.email}`,
      subject: `Someone shared a post with you!`,
      text: `Hello.
      ${user.username} shared a post with you.
      Have a look: ${body.postLink}`,
    });
    // console.log("Message sent: %s", info.messageId);
    return info;
  }

  async notifyByEmail(post, postUser, likeUsername, isLike) {
    const liked = isLike ? "liked" : "disliked";
    let info = await transporter.sendMail({
      from: 'thread-js',
      to: `${postUser.email}`,
      subject: `Your post was ${liked}!`,
      text: `Hello
      Your post was liked by ${likeUsername}.
      Post: ${post.body}`,
    });
    // console.log("Message sent: %s", info.messageId);
    return info;
  }

  async setReaction(userId, { postId, isLike = true }) {
    // define the callback for future use as a promise

    const updateOrDelete = react => (react.isLike === isLike
      ? this._postReactionRepository.deleteById(react.id)
      : this._postReactionRepository.updateById(react.id, { isLike }));
    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );
    let creation;
    if(reaction){
      if(reaction.isLike !== isLike){
        this._postReactionRepository.deleteById(reaction.id);
        creation = await this._postReactionRepository.create({ userId, postId, isLike });
        return {
          id: postId,
          hadReact: true,
          isLike: isLike
        };
      }
    }

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._postReactionRepository.getPostReaction(userId, postId);
  }
}

export { Post };
