class Comment {
  constructor({ commentRepository, commentReactionRepository }) {
    this._commentRepository = commentRepository;
    this._commentReactionRepository = commentReactionRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  deleteCommentById(id){
    return this._commentRepository.softDeleteCommentById(id);
  }

  edit(commentId, body) {
    return this._commentRepository.editCommentById(commentId, body);
  }

  async getLikeListById(id){
    const likeList = await this._commentReactionRepository.getLikeListById(id);
    let likeListUsernames = [];
    for(let i=0; i<likeList.length; i++){
      likeListUsernames.push(likeList[i].dataValues.userId);
    }
    return likeListUsernames;
  }

async setReaction(userId, { commentId, isLike = true }) {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? this._commentReactionRepository.deleteById(react.id)
    : this._commentReactionRepository.updateById(react.id, { isLike }));
  const reaction = await this._commentReactionRepository.getCommentReaction(
    userId,
    commentId
  );
  let creation;

  if (reaction) {
    if (reaction.isLike !== isLike) {
      this._commentReactionRepository.deleteById(reaction.id);
      creation = await this._commentReactionRepository.create({ userId, commentId, isLike });
      return {
        id: commentId,
        hadReact: true
      };
    }
  }
  const result = reaction
    ? await updateOrDelete(reaction)
    : await this._commentReactionRepository.create({ userId, commentId, isLike });
  // the result is an integer when an entity is deleted
  return Number.isInteger(result)
    ? {}
    : this._commentReactionRepository.getCommentReaction(userId, commentId);
  }
}

export { Comment };
