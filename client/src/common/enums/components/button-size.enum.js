const ButtonSize = {
  LARGE: 'large',
  MEDIUM: 'medium',
  MINI: 'mini',
  TINY: 'tiny',
  SMALL: 'small'
};

export { ButtonSize };
