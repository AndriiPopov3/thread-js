import * as React from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { IconName, IconColor } from 'src/common/enums/enums';
import { Icon, Modal, Input, Button } from 'src/components/common/common';
import { threadActionCreator } from 'src/store/actions';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close }) => {
  const [isCopied, setIsCopied] = React.useState(false);
  const [isShared, setIsShared] = React.useState(false);
  const [emailBody, setEmail] = React.useState(undefined);
  const [emailShare, setEmailShare] = React.useState(false);
  let input = React.useRef();
  const dispatch = useDispatch();
  const copyToClipboard = ({ target }) => {
    input.select();
    document.execCommand('copy');
    target.focus();
    setIsCopied(true);
  };

  const shareByEmail = () => {
    if (emailBody) {
      dispatch(threadActionCreator.shareByEmail(emailBody, `${window.location.origin}/share/${postId}`));
      setIsShared(true);
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {isCopied && (
          <span>
            <Icon name={IconName.COPY} color={IconColor.GREEN} />
            Copied
          </span>
        )}
        {isShared && (
          <span>
            <Icon name={IconName.MAIL} color={IconColor.GREEN} />
            Shared
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={`${window.location.origin}/share/${postId}`}
          ref={ref => {
            input = ref;
          }}
        />
        <br />
        <Button color="teal" onClick={() => (setEmailShare(!emailShare))}>Share by email</Button>
        <br />
        <br />
        {emailShare
        && (
          <Input
            fluid
            action={{
              color: 'teal',
              labelPosition: 'right',
              icon: 'mail',
              content: 'Share',
              onClick: shareByEmail
            }}
            placeholder="Enter email"
            onChange={event => setEmail(event.target.value)}
          />
        )}
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default SharedPostLink;
