import * as React from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Icon, Label, Input, Button, Comment as CommentUI } from 'src/components/common/common';
import { IconName } from 'src/common/enums/enums';
import { commentType, userType } from 'src/common/prop-types/prop-types';
import { threadActionCreator } from 'src/store/actions';

import styles from './styles.module.scss';

const Comment = ({ comment, currentUser, onCommentLike, onCommentDislike, deleteComment, editComment }) => {
  const {
    id,
    body,
    user,
    likeCount,
    dislikeCount,
    createdAt,
    likes
  } = comment;
  const dispatch = useDispatch();
  // const date = getFromNowTime(createdAt);
  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);

  const [commentEditable, setCommentEditable] = React.useState(false);
  const [editedCommentBody, setEditedCommentBody] = React.useState(body);
  const [commentDelete, setCommentDelete] = React.useState(false);
  const [likeListShown, setLikeListShown] = React.useState(false);

  const handleLikeList = () => {
    setLikeListShown(!likeListShown);
    dispatch(threadActionCreator.getLikeListComment(id));
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        <br />
        {user.status ? (<CommentUI.Metadata>{user.status}</CommentUI.Metadata>) : null}
        <CommentUI.Text>
          {!commentEditable && editedCommentBody}
          {commentEditable
          && (
            <Input
              fluid
              transparent={commentEditable ? '' : 'transparent'}
              value={editedCommentBody}
              readOnly={commentEditable ? '' : 'readOnly'}
              onChange={event => setEditedCommentBody(event.target.value)}
            />
          )}
          {commentEditable
            && (
              <Button
                size="small"
                isBasic
                floated="right"
                color="green"
                onClick={() => {
                  if (editedCommentBody !== '') {
                    editComment({ commentId: id, body: `${editedCommentBody}` });
                  } else {
                    setEditedCommentBody(body);
                  }
                  setCommentEditable(false);
                }}
              >
                Save
              </Button>
            )}
          {commentEditable
            && (
              <Button
                size="small"
                isBasic
                floated="right"
                color="blue"
                onClick={() => {
                  setCommentEditable(false);
                  setEditedCommentBody(body);
                }}
              >
                Cancel
              </Button>
            )}
          {commentDelete
            && (
              <Button
                size="small"
                isBasic
                floated="right"
                color="red"
                onClick={() => {
                  deleteComment(id);
                  setCommentEditable(false);
                }}
              >
                Delete
              </Button>
            )}
          {commentDelete
            && (
              <Button
                size="small"
                isBasic
                floated="right"
                color="blue"
                onClick={() => setCommentDelete(false)}
              >
                Cancel
              </Button>
            )}
        </CommentUI.Text>
        <CommentUI.Actions style={{ display: 'flex', alignItems: 'top', justifyContent: 'top' }}>
          <CommentUI.Action
            onClick={handleCommentLike}
            onMouseEnter={handleLikeList}
            onMouseLeave={handleLikeList}
          >
            <Icon name={IconName.THUMBS_UP} />
            {likeCount}
          </CommentUI.Action>
          {likeListShown
            && (
              <Label
                pointing="left"
                color="teal"
              >
                {(likes && likes.length !== 0)
                  ? likes.map(like => (<div>{like}</div>)) : (<div>No likes yet</div>)}
              </Label>
            )}
          <CommentUI.Action onClick={handleCommentDislike}>
            <Icon name={IconName.THUMBS_DOWN} />
            {dislikeCount}
          </CommentUI.Action>
          {
            currentUser === user.id ? (
              <CommentUI.Action onClick={() => setCommentEditable(true)}>
                <Icon name={IconName.EDIT} />
              </CommentUI.Action>
            ) : null
          }
          {
            currentUser === user.id ? (
              <CommentUI.Action onClick={() => setCommentDelete(true)}>
                <Icon name={IconName.DELETE} />
              </CommentUI.Action>
            ) : null
          }
        </CommentUI.Actions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  currentUser: userType.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired
};

export default Comment;
