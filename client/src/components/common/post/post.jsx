import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType, userType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label, Input, Button } from 'src/components/common/common';
import { threadActionCreator } from 'src/store/actions';

import styles from './styles.module.scss';

const Post = ({ post, currentUser, onPostLike, onPostDislike, onExpandedPostToggle, sharePost, deletePost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    likes
  } = post;

  const date = getFromNowTime(createdAt);

  const [likeListShown, setLikeListShown] = React.useState(false);
  const [postEditable, setPostEditable] = React.useState(false);
  const [postDelete, setPostDelete] = React.useState(false);
  const [editedPostBody, setEditedPostBody] = React.useState(body);
  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const dispatch = useDispatch();

  const handleLikeList = () => {
    setLikeListShown(!likeListShown);
    dispatch(threadActionCreator.getLikeList(id));
  };

  const handlePostEdit = () => {
    if (postEditable) {
      if (editedPostBody !== '') {
        dispatch(threadActionCreator.editPost({ postId: id, body: `${editedPostBody}` }));
      } else {
        setEditedPostBody(body);
      }
    }
    setPostEditable(false);
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {!postEditable && editedPostBody}
          {postEditable
          && (
            <Input
              fluid
              transparent={postEditable ? '' : 'transparent'}
              value={editedPostBody}
              readOnly={postEditable ? '' : 'readOnly'}
              onChange={event => setEditedPostBody(event.target.value)}
            />
          )}
          <br />
          {postEditable
            && (
              <Button
                size="small"
                isBasic
                floated="right"
                color="green"
                onClick={() => handlePostEdit()}
              >
                Save
              </Button>
            )}
          {postEditable
            && (
              <Button
                size="small"
                isBasic
                floated="right"
                color="blue"
                onClick={() => {
                  setPostEditable(false);
                  setEditedPostBody(body);
                }}
              >
                Cancel
              </Button>
            )}
          {postDelete
            && (
              <Button
                size="small"
                isBasic
                floated="right"
                color="red"
                onClick={() => deletePost(id)}
              >
                Delete
              </Button>
            )}
          {postDelete
            && (
              <Button
                size="small"
                isBasic
                floated="right"
                color="blue"
                onClick={() => setPostDelete(false)}
              >
                Cancel
              </Button>
            )}
        </Card.Description>
      </Card.Content>
      <Card.Content extra style={{ display: 'flex', alignItems: 'top', justifyContent: 'top' }}>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostLike}
          onMouseEnter={handleLikeList}
          onMouseLeave={handleLikeList}
        >
          <Icon name={IconName.THUMBS_UP} />
          {likeCount}
        </Label>
        {likeListShown
        && (
          <Label
            pointing="left"
            color="teal"
          >
            {(likes && likes.length !== 0)
              ? likes.map(like => (<div>{like}</div>)) : (<div>No likes yet</div>)}
          </Label>
        )}
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
        {
          currentUser === user.id ? (
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => setPostEditable(true)}
            >
              <Icon name={IconName.EDIT} />
            </Label>
          ) : null
        }
        {
          currentUser === user.id ? (
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => setPostDelete(true)}
            >
              <Icon name={IconName.DELETE} />
            </Label>
          ) : null
        }
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  currentUser: userType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default Post;
