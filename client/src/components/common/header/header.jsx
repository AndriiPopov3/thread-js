import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { IconName, IconSize, ButtonType, AppRoute } from 'src/common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { userType } from 'src/common/prop-types/prop-types';
import { profileActionCreator } from 'src/store/actions';
import {
  Button,
  Icon,
  Image,
  Grid,
  NavLink,
  Input
} from 'src/components/common/common';

import styles from './styles.module.scss';

const Header = ({ user, onUserLogout }) => {
  const [statusEditable, setStatusEditable] = React.useState(false);
  const [body, setBody] = React.useState(user.status);
  const dispatch = useDispatch();
  const handleStatusEdit = () => {
    setStatusEditable(!statusEditable);
    if (statusEditable) {
      if (body.length < 255) {
        dispatch(profileActionCreator.setStatus({ body: `${body}` }));
      }
    }
  };

  return (
    <div className={styles.headerWrp}>
      <Grid centered container justify="center" columns="2">
        <Grid.Column>
          {user && (
            <NavLink exact to={AppRoute.ROOT}>
              <div className={styles.userWrapper}>
                <Image
                  circular
                  width="45"
                  height="45"
                  src={user.image?.link ?? DEFAULT_USER_AVATAR}
                />
                {' '}
                {user.username}
              </div>
            </NavLink>
          )}
          <br />
          <Button color="teal" iconName="edit" size="mini" onClick={() => handleStatusEdit()} />
          <Input
            transparent={statusEditable ? '' : 'transparent'}
            defaultValue={body}
            placeholder="Status"
            readOnly={statusEditable ? '' : 'readOnly'}
            onChange={event => setBody(event.target.value)}
          />
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink
            exact
            activeClassName="active"
            to={AppRoute.PROFILE}
            className={styles.menuBtn}
          >
            <Icon name={IconName.USER_CIRCLE} size={IconSize.LARGE} />
          </NavLink>
          <Button
            className={`${styles.menuBtn} ${styles.logoutBtn}`}
            onClick={onUserLogout}
            type={ButtonType.BUTTON}
            iconName={IconName.LOG_OUT}
            iconSize={IconSize.LARGE}
            isBasic
          />
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  onUserLogout: PropTypes.func.isRequired,
  user: userType.isRequired
};

export default Header;
