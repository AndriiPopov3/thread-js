import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid, Image, Input, Form, Button } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { profileActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';

const Profile = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const [submitted, setSubmitted] = React.useState(true);
  const [usernameSubmitted, setUsernameSubmitted] = React.useState(true);
  const [body, setBody] = React.useState(user.status);
  const [usernameBody, setUsernameBody] = React.useState(user.username);
  const [isUploading, setIsUploading] = React.useState(false);
  const [photoWasChanged, setPhotoWasChanged] = React.useState(false);
  const [image, setImage] = React.useState(undefined);
  const dispatch = useDispatch();

  const uploadImage = file => imageService.uploadImage(file);

  const statusChangeHandler = () => {
    setSubmitted(!submitted);
    if (!submitted) {
      if (body.length < 255) {
        dispatch(profileActionCreator.setStatus({ body: `${body}` }));
      } else {
        setBody(user.status);
      }
    }
  };

  const usernameChangeHandler = () => {
    setUsernameSubmitted(!usernameSubmitted);
    if (!usernameSubmitted) {
      if (usernameBody.length < 255) {
        dispatch(profileActionCreator.setUsername({ body: `${usernameBody}` }));
      } else {
        setUsernameBody(user.username);
      }
    }
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
        dispatch(profileActionCreator.setPhoto({ imageId, imageLink }));
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
        setPhotoWasChanged(true);
      });
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image
          centered
          src={!photoWasChanged ? (user.image?.link ?? (image?.imageLink ?? DEFAULT_USER_AVATAR)) : image?.imageLink}
          size="medium"
          circular
        />
        <br />
        <Button
          color="teal"
          isLoading={isUploading}
          isDisabled={isUploading}
        >
          <label>
            Change photo
            <input
              name="image"
              type="file"
              onChange={handleUploadFile}
              hidden
            />
          </label>
        </Button>
        <br />
        <br />
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}
        >
          <Form onSubmit={usernameChangeHandler}>
            <Form.Group alignItems="center">
              <Form.Input
                icon="user"
                iconPosition="left"
                placeholder="Username"
                type="text"
                disabled={usernameSubmitted ? 'disabled' : ''}
                onChange={event => setUsernameBody(event.target.value)}
                value={usernameBody}
              />
              <Form.Button color="teal" type="submit" content="Change username" />
            </Form.Group>
          </Form>
        </div>
        <br />
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}
        >
          <Form onSubmit={statusChangeHandler}>
            <Form.Group alignItems="center">
              <Form.Input
                icon="pencil"
                iconPosition="left"
                placeholder="Status"
                type="text"
                disabled={submitted ? 'disabled' : ''}
                onChange={event => setBody(event.target.value)}
                value={body}
              />
              <Form.Button color="teal" type="submit" content="Change status" />
            </Form.Group>
          </Form>
        </div>
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
      </Grid.Column>
    </Grid>
  );
};

export default Profile;
