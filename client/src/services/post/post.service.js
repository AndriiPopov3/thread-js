import { HttpMethod, ContentType } from 'src/common/enums/enums';
// import * as nodemailer from 'nodemailer';

class Post {
  constructor({ http }) {
    this._http = http;
  }

  getAllPosts(filter) {
    return this._http.load('/api/posts', {
      method: HttpMethod.GET,
      query: filter
    });
  }

  getPost(id) {
    return this._http.load(`/api/posts/${id}`, {
      method: HttpMethod.GET
    });
  }

  addPost(payload) {
    return this._http.load('/api/posts', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  softDeletePost(id) {
    return this._http.load(`/api/posts/${id}`, {
      method: HttpMethod.DELETE
    });
  }

  likePost(postId) {
    return this._http.load('/api/posts/react', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: true
      })
    });
  }

  dislikePost(postId) {
    return this._http.load('/api/posts/react', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: false
      })
    });
  }

  getLikeList(id) {
    return this._http.load(`/api/posts/likeList/${id}`, {
      method: HttpMethod.GET
    });
  }

  editPost(id, payload) {
    return this._http.load(`/api/posts/edit/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  shareByEmail(email, postLink) {
    return this._http.load('/api/posts/share', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        email,
        postLink
      })
    });
  }
}

export { Post };
