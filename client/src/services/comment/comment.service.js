import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Comment {
  constructor({ http }) {
    this._http = http;
  }

  getComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  addComment(payload) {
    return this._http.load('/api/comments', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  likeComment(commentId) {
    return this._http.load('/api/comments/react', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        isLike: true
      })
    });
  }

  dislikeComment(commentId) {
    return this._http.load('/api/comments/react', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        isLike: false
      })
    });
  }

  softDeleteComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.DELETE
    });
  }

  editComment(id, payload) {
    return this._http.load(`/api/comments/edit/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  getLikeList(id) {
    return this._http.load(`/api/comments/likeList/${id}`, {
      method: HttpMethod.GET
    });
  }
}

export { Comment };
