import { HttpMethod, ContentType } from 'src/common/enums/enums';

class User {
  constructor({ http }) {
    this._http = http;
  }

  setStatus(payload) {
    return this._http.load('/api/user/status', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  setUsername(payload) {
    return this._http.load('/api/user/username', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  setPhoto(payload) {
    return this._http.load('/api/user/photo', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
}

export { User };
