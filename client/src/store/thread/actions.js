import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  SET_ALL_COMMENTS: 'thread/set-all-comments'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const setComments = createAction(ActionType.SET_ALL_COMMENTS, comments => ({
  payload: {
    comments
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const shareByEmail = (email, postLink) => async () => {
  await postService.shareByEmail(email, postLink);
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
  if (post) {
    dispatch(setComments(post.comments));
  }
};

const deletePost = post => async dispatch => {
  await postService.softDeletePost(post);
  const posts = await postService.getAllPosts();
  dispatch(setPosts(posts));
  dispatch(setExpandedPost(undefined));
};

const editPost = editedPost => async dispatch => {
  await postService.editPost(editedPost.postId, editedPost);
  const posts = await postService.getAllPosts();
  dispatch(setPosts(posts));
};

const editComment = editedComment => async (dispatch, getRootState) => {
  await commentService.editComment(editedComment.commentId, editedComment);
  const comment = await commentService.getComment(editedComment.commentId);
  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || [])]
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const deleteComment = comment => async (dispatch, getRootState) => {
  const commentToDelete = await commentService.getComment(comment);
  await commentService.softDeleteComment(comment);
  const mapComments = post => ({ // fix visual update here
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: [...(post.comments?.map(com => (com === commentToDelete ? null : com)) || [])]
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== commentToDelete.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === commentToDelete.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const likePost = postId => async (dispatch, getRootState) => {
  const { id, hadReact } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const diffDislike = hadReact ? -1 : 0; // if hadReact exists then the post was disliked
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + diffDislike // dislike is taken away if existed and like was pressed
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const { id, hadReact } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1;
  const diffLike = hadReact ? -1 : 0;
  const mapDislikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diffLike,
    dislikeCount: Number(post.dislikeCount) + diff
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapDislikes(expandedPost)));
  }
};

const getLikeList = postId => async (dispatch, getRootState) => {
  const likeList = await postService.getLikeList(postId);
  const mapLikes = post => ({
    ...post,
    likes: likeList
  });
  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const getLikeListComment = commentId => async (dispatch, getRootState) => {
  const likeList = await commentService.getLikeList(commentId);
  const mapLikes = comment => ({
    ...comment,
    likes: likeList
  });

  const {
    comments: { comments }
  } = getRootState();
  const updatedComments = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));

  dispatch(setComments(updatedComments));
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const likeComment = commentId => async (dispatch, getRootState) => {
  const { id, hadReact } = await commentService.likeComment(commentId);
  const likedComment = await commentService.getComment(commentId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const diffDislike = hadReact ? -1 : 0;

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(comment.dislikeCount) + diffDislike
  });

  const {
    comments: { comments }
  } = getRootState();
  const updatedComments = comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));

  dispatch(setComments(updatedComments));

  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || [])]
  });

  const {
    posts: { expandedPost }
  } = getRootState();

  if (expandedPost && expandedPost.id === likedComment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { id, hadReact } = await commentService.dislikeComment(commentId);
  const likedComment = await commentService.getComment(commentId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const diffLike = hadReact ? -1 : 0;
  const mapDislikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diffLike,
    dislikeCount: Number(comment.dislikeCount) + diff // diff is taken from the current closure
  });

  const {
    comments: { comments }
  } = getRootState();
  const updatedComments = comments.map(comment => (comment.id !== commentId ? comment : mapDislikes(comment)));

  dispatch(setComments(updatedComments));

  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || [])]
  });

  const {
    posts: { expandedPost }
  } = getRootState();

  if (expandedPost && expandedPost.id === likedComment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  setComments,
  addMorePosts,
  addPost,
  setExpandedPost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  shareByEmail,
  toggleExpandedPost,
  deletePost,
  editPost,
  editComment,
  deleteComment,
  likePost,
  dislikePost,
  getLikeList,
  getLikeListComment,
  addComment,
  likeComment,
  dislikeComment
};
